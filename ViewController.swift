//
//  ViewController.swift
//  IoT Car Park
//
//  Created by Onurcan Önder on 05/12/17.
//  Copyright © 2017 Onurcan Önder. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var userName: UITextField!
    static var userLabel = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func signInButton() {
        ViewController.userLabel = userName.text!
        print(ViewController.userLabel)
    }
    
}

