//
//  ViewController2.swift
//  IoT Car Park
//
//  Created by Onurcan Önder on 05/12/17.
//  Copyright © 2017 Onurcan Önder. All rights reserved.
//

import UIKit
import CoreLocation

class ViewController2: UIViewController, CLLocationManagerDelegate {
    
    @IBOutlet weak var actionLabel: UILabel!
    
    // BLE Elements
    var prevBeacon = 0
    var proximityCounter = 0
    let locationManager = CLLocationManager() // Bluetooth
    let region = CLBeaconRegion(proximityUUID: UUID(uuidString:"B9407F30-F5F8-466E-AFF9-25556B57FE6D")!, identifier: "Estimotes")
    let beaconIDs = [2592: 1, 60174: 2, 56188: 3]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.startRangingBeacons(in: region)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func locationManager(_ manager: CLLocationManager, didRangeBeacons beacons: [CLBeacon], in region: CLBeaconRegion) {
        print(beacons)
        let knownBeacons = beacons.filter{$0.proximity != CLProximity.unknown}
        if(knownBeacons.count > 0) {
            let closestBeacon = knownBeacons[0] as CLBeacon
            let number = self.beaconIDs[closestBeacon.minor.intValue]!
            print("Test:"+"\(closestBeacon.rssi)")
            //proximity.text = "\(closestBeacon.rssi)"
            if(closestBeacon.rssi >= -80 && number != prevBeacon){
                if(number == 2) {
                    //Turquoise
                    actionLabel.text = ViewController.userLabel + "has arrived slot 1"
                } else if(number == 3) {
                    //Green
                    actionLabel.text = ViewController.userLabel + "has arrived slot 2"
                }
                proximityCounter+=1
                prevBeacon = number
            }
        }
    }
    
    @IBAction func slot1Button(_ sender: UIButton) {
        sender.setTitleColor(UIColor.red, for: UIControlState.normal)
        actionLabel.text = "Slot 1 occupied by " + ViewController.userLabel
    }
    
    @IBAction func slot2Button(_ sender: UIButton) {
        sender.setTitleColor(UIColor.red, for: UIControlState.normal)
        actionLabel.text = "Slot 2 occupied by " + ViewController.userLabel
    }
    
    
}
